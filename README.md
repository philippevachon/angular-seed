# Angular Seed

Barebones template for quick testing and prototyping on Angular

Includes Twitter Bootstrap and FontAwesome and a few grunt tasks to compile bootstrap, serve the site and watch the files for any modifications to livereload.

NOTICE : Do not use this for production or large projects. In those cases you are better off building your project with yeoman's angular generator which includes everything you need to ship tight code and optimized assets to production. 

## Usage 

1. Fork the repo
2. Run `npm install`
3. Run `grunt serve` at the root of the project
4. Start coding! The site will livereload when modifications are made to the files

## Deploying to Heroku

1. Download the Heroku toolbelt 
2. Run `heroku create` to create a new heroku application
3. Run `git push heroku master` to send your files to heroku
4. Run `heroku ps:scale web=1` 
4. Run `heroku open` to see your app in action

## TODO

- Include a few important angular plugins, like ui-router, ng-ressource, etc.
- Boilerplate for testing