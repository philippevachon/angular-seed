module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    connect: {
    	server: {
    		options: {
    			hostname: 'localhost',
    			livereload: 35729,
    			port: 9001,
    			base: 'app',
    			open: 'http://localhost:9001/'
    		}
    	}
    },
    watch: {
    	options: {
	      livereload: 35729,
	    },
    	bootstrap: {
    		files: [
    			'app/bower_components/bootstrap/**/*',
    			'app/bower_components/font-awesome/**/*',
    		],
    		tasks: ['less:bootstrap'],
    	},
	    css: {
			files: ['app/css/**/*'],
			tasks: [],
	    },
	    less: {
	    	files: ['app/less/**/*'],
	    	tasks: ['less:main']
	    },
	    js: {
	    	files: ['app/js/**/*'],
	    	tasks: []
	    },
	    templates: {
	    	files: ['app/templates/**/*'],
	    	tasks: []
	    },
	    index: {
	    	files: ['app/index.html'],
	    	tasks: []
	    }
	},
	less: {
		bootstrap: {
			options: {
				compress: true,
				modifyVars: {
					'fa-font-path': '"../bower_components/font-awesome/fonts"'
				}
			},
			files: {
				'app/css/bootstrap-fontawesome.min.css': [
					'app/bower_components/bootstrap/less/bootstrap.less',
					'app/bower_components/font-awesome/less/font-awesome.less',
				]
			}
		},
		main: {
			options: {
				compress: false,
				strictImports: true
			},
			files: {
				'app/css/main.css': [
					'app/less/app.less'
				]
			}
		}
	}
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');

  // Default task(s).
  grunt.registerTask('serve', ['connect', 'watch']);
  grunt.registerTask('build', ['less']);
};